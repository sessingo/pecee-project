<?php
require_once '../config/init.php';

// Route request
try {
    \Pecee\Router::GetInstance()->routeRequest();
}catch(\Exception $e) {
    throw $e;
}