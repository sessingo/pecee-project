<?php
namespace Demo\Controller;
use Demo\Widget\WidgetHome;
use Pecee\Controller;

class ControllerDefault extends Controller {
	public function indexView() {
		echo new WidgetHome();
	}
}